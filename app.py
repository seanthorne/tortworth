from flask import Flask
from flask import render_template, flash, url_for, redirect, request, abort, session, jsonify

import requests
import os
import json
import uuid

from flask_login import LoginManager, login_user, current_user, logout_user, login_required
from flask_mail import Message, Mail
from werkzeug import secure_filename



# from utils import get_dad_joke, nice_date
from flask import Flask
from flask_sqlalchemy import SQLAlchemy, SessionBase
from flask_migrate import Migrate

import datetime
from datetime import timedelta



import os

app = Flask(__name__)

db = SQLAlchemy(app)

from models import User, UploadedImage, Post
import forms
import utils

app.secret_key = 'sean'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
migrate = Migrate(app, db)

def redirect_url(default='index'):
    return request.args.get('next') or \
           request.referrer or \
           url_for(default)


####### Mail

os.environ['GMAIL'] = "tortworthmatrix@gmail.com"
os.environ['PASSWORD'] = "manager-1"

app.config.update(dict(
    MAIL_SERVER='smtp.googlemail.com',
    MAIL_PORT=465,
    MAIL_USE_TLS=False,
    MAIL_USE_SSL=True,
    MAIL_USERNAME=os.environ.get('GMAIL'),
    MAIL_PASSWORD=os.environ.get('PASSWORD')
))


@app.route('/entry/<date>', methods=['GET', 'POST'])
def stream(date):
    dt = datetime.datetime.strptime(date, '%Y-%m-%d')
    form = forms.NewPost()
    if form.validate_on_submit():
        new_post = Post(
            body=form.body.data,
            user_id=current_user.id,
            sentiment=form.sentiment.data,
            department=form.department.data,
            guest_name=form.guest_name.data,
            arrival_date=form.arrival_date.data,
            timestamp=dt.date())
        print(type(dt))
        
        db.session.add(new_post)
        db.session.commit()
        return redirect(url_for('pick_log_date', date=dt.date()))
    
    return render_template('posts.html', date=dt.date(), form=form)

@app.route('/log/today')
def todays_log():
    posts = Post.query.filter_by(timestamp=datetime.date.today() ).all()
 
    return render_template('log.html' , posts=posts, date=datetime.date.today())

@app.route('/log/yesterday')
def yesterdays_log():
    posts = Post.query.filter_by(timestamp=datetime.date.today() - timedelta(days=1)).all()
 
    return render_template('log.html' , posts=posts, date=datetime.date.today() - timedelta(days=1))

@app.route('/log', methods=['POST', 'GET'])
def pick_log_date():
    if request.form:
        date = request.form['date']
    else:
        date = request.args.get('date')
    dt = datetime.datetime.strptime(str(date), '%Y-%m-%d')
    posts = Post.query.filter_by(timestamp=dt.date()).all()   
    return render_template('log.html', posts=posts, date=dt.date())

# @app.route('/log', methods=['POST', 'GET'])
# def date_log(date=None):
#     if request.form['date'] == '':
#         flash('You need to select a date you want to change to.')
#         return redirect(request.referrer)
#     data = datetime.datetime.strptime(request.form['date'], '%Y-%m-%d')
#     try:
#         date = data
#     except ValueError:
#         flash('Date must be entered as dd-mm-yyyy including dashes, you could click the calendar icon also.', category='error')
#         return redirect(url_for('todays_log'))
#     posts = Post.query.filter_by(timestamp=date.date()).all()  
#     return render_template('log.html' , posts=posts, date=date)

@app.route('/deletePost/<post_id>')
def delete_post(post_id):
    post_to_delete = Post.query.get(post_id)
    db.session.delete(post_to_delete)
    db.session.commit()
    return (' ', 204)
    
    # timestamp = datetime.strptime(str(post_to_delete.timestamp), "%d%m%Y")
    # return redirect(url_for('todays_log'))

#######################
# Stats ###############
#######################

@app.route('/stats/overall')
def overall_stats():
    month = datetime.datetime.today().month
    date_from = datetime.datetime(2018, 10, 1)
    date_to = datetime.datetime(2018, 10, 31)
    # delta = date_to - date_from
    # the_key = []
    # for i in range(1, delta.days + 1):
    #     the_key.append(i)
    #     print(i)
    def inclusive_date_range(start: datetime, end: datetime, increment = timedelta(days=1)):
        while start <= end:
            # the_key.append(start)
            yield start
            start += increment  
    
    the_real_key = []
    the_key = [x.strftime('%d-%b') for x in inclusive_date_range(date_from, date_to)]
    for x in the_key:
        the_real_key.append(str(x))

    posts = Post.query.filter(db.between(Post.timestamp, date_from, date_to)).all()
    from collections import defaultdict
    payload = defaultdict(list)
    for post in posts:
        date = post.timestamp.day
        payload[date].append(post)
    key = []
    value = []
    for k, v in payload.items():
        key.append(k)
        value.append(len(v))
        print(f'{k}' + ' -' + str(len(v)))
    print(the_real_key)
    return render_template('overall_stats.html', 
    the_key=[x.date().day for x in inclusive_date_range(date_from, date_to)],
    # the_key=the_real_key,
    the_data=value)


@app.route('/me')
def my_stream():
    form = forms.NewPost()
    users_posts = Post.query.filter_by(user_id=current_user.id)
    return render_template('posts.html', posts=users_posts, form=form)

@app.route('/user/<user_id>')
def users_stream(user_id):
    user = User.query.get(user_id)
    posts = Post.query.filter_by(user_id=user.id)
    return render_template('posts.html', user=user, posts=posts, form=forms.NewPost())

@app.route('/user/profile')
def user_profile():
    user_id = request.args.get('user')
    user = User.query.get(user_id)
    users_posts = Post.query.filter_by(user_id=user_id)
    return render_template(
        'user_posts.html',
        user = user,
        total_posts=users_posts.count(),
        posts=users_posts)
##################
# Error Handlers #
##################

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 404

@app.route('/construction')
@login_required
def construction():
    flash('This page is not quite finished', category='warning')
    return redirect(url_for('home'))

#################
# Login Manager #
#################
login = LoginManager(app)
login.login_view = "check_password"

@app.route('/')
@app.route('/index')
def home():
    if current_user.is_authenticated:
        return redirect(url_for('todays_log'))
    else:
        return redirect(url_for('check_password'))

@login.user_loader
def load_user(id): 
    return User.query.get(int(id))

@app.route('/login', methods=['POST', 'GET'])
def check_password():
    """
    Login for users. Takes POST and GET. 
    Get checks if user is logged in and returns home() if True, else.
    POST adds new user to db and returns home() also.
    """
    if current_user.is_authenticated:
        return home()
    
    form = forms.LoginUser()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.email.data.lower()).first()
        if user is not None and user.password == form.password.data:
            login_user(user=user)
            next = request.args.get('next')
            return redirect(next or url_for('home'))
        else:
            flash('Username or password are incorrect.', category='error')
            return redirect(url_for('check_password'))
    return render_template('password.html', form=form, greeting=utils.greeting())

    # if request.method == 'POST' and form.validate():
    #     name = request.form['email']
    #     raw_username = name.split('.')
    #     username = raw_username[0]
    #     passw = request.form['password']
    #     data = User.query.filter_by(
    #         username=name.lower(), password=passw).first()
    #     if data is not None:
    #         session['logged_in'] = True
    #         session['username'] = username.title()
    #         session['id'] = data.id
    #         session['email'] = data.username
    #         return home()
    #     else:
    #         flash('User not found.')
    #         return render_template('password.html', form=form, greeting=utils.greeting())
    # else:
    #     return render_template('password.html', form=form, greeting=utils.greeting())

    # return home()

@app.route("/logout")
def logout():
    """Logout Form"""
    logout_user()
    flash('Successfully logged out.', category='warning')
    return home()

@app.route('/register', methods=['GET', 'POST'])
def register():
    """Register Form"""
    form = forms.RegisterUser()

    if request.method == 'POST' and form.validate():
        nickname = request.form['email'].split('@')[0]
        nickname = nickname.split('.')
        nickname = f'{nickname[0].title()} {nickname[1].title()}'
        new_user = User(
            username=request.form['email'].lower(),
            password=request.form['password'],
            job_title=request.form['job_title'],
            nickname=nickname,
            admin=False)

    try:
        db.session.add(new_user)
        db.session.commit()
        session['logged_in'] = True
        return render_template('register_success.html')
    except:
        flash(
        'Hmm. I seem to have that email address already, do you need to recover your password?')

    return render_template('register.html', form=form)

@app.route('/static/<json_file>.json')
def static_folder(json_file):
    return home()

############
# Handbook #
############
@app.route('/handbook')
@login_required
def handbook():
    return render_template('handbook.html', logout=True)

##############
# Local Area #
##############
@app.route('/walking')
@login_required
def walking_routes():
    return redirect(url_for('construction'))

@app.route('/travel')
@login_required
def travel():
    return redirect(url_for('construction'))


@app.route('/thingstodo')
@login_required
def things_to_do():
    return redirect(url_for('construction'))


@app.route('/update', methods=['GET', 'POST'])
@login_required
def update_user():
    form = forms.UpdateUser()
    user = User.query.get(current_user.id)

    if request.method == 'POST' and form.validate():
        user.username = request.form['email']
        user.password = request.form['password']
        user.job_title = request.form['job_title']
        user.nickname = request.form['nickname']
        try:
            db.session.commit()
        except:
            db.session.rollback()
            flash('Email in use already.')
            return home()
        flash('{} updated.'.format(user.username))
        return home()
    else:
        return render_template('user_edit.html', form=form, user=user)

@app.route('/upload/<room_number>', methods=['GET', 'POST'])
@login_required
def upload_file(room_number):   
    form = forms.UploadFile()

    all_images = UploadedImage.query.filter_by(room=room_number).all()
    length = len(all_images)
    file_paths = []
    for x in all_images:
        file_paths.append(f"/static/uploads/{x.file_name}")

    if length == 0:
        file_paths = False

    if form.validate_on_submit():

        f = request.files.getlist('file')
        for image in f:
            image_file_name = f'xxx{room_number}xxx' + \
                str(uuid.uuid4()) + secure_filename(image.filename)
            image_file = os.path.join(os.path.abspath(
                'static/uploads'), image_file_name)
            image.save(image_file)

            image_upload = UploadedImage(
                room=room_number,
                file_name=image_file_name,
                file_path=image_file
            )
            db.session.add(image_upload)

        db.session.commit()
        db.session.close()
        flash('Images added.')
        return room(room_number)

    return render_template('upload.html', room=room_number, images=all_images, form=form)


@app.route('/upload/delete')
@login_required
def delete_file():
    image_id = request.args.get('image_id')
    image_to_delete = UploadedImage.query.get(image_id)
    db.session.delete(image_to_delete)
    db.session.commit()
    flash('Image deleted.')
    return room(request.args.get('room'))


## Admin

@app.route("/admin")
@login_required
def admin_panel():
    admin = User.query.get(current_user.id)
    if not admin.admin:
        flash("You can't do that. Soz.")
        return home()

    users = User.query.all()
    return render_template('admin.html', users=users)

@app.route("/admin/update/<user_id>", methods=['GET', 'POST'])
@login_required
def admin_update_user(user_id):
    form = forms.UpdateUser()
    user = User.query.get(user_id)

    if request.method == 'POST' and form.validate():
        print(request.form)
        user.username = request.form['email']
        user.password = request.form['password']
        user.job_title = request.form['job_title']
        user.nickname = request.form['nickname']
        if request.form.get('admin') == 'y':
            user.admin = 1
        else:
            user.admin = 0
        try:
            db.session.commit()
        except:
            db.session.rollback()
            flash('Email in use already.')
            return admin_panel()
        flash(f'{user.username} updated.')
        return admin_panel()
    else:
        return render_template('admin_edit.html', form=form, user=user)


@app.route('/admin/delete/<user_id>')
def admin_delete(user_id):
    user = User.query.get(user_id)
    db.session.delete(user)
    db.session.commit()
    flash('Deleted {}'.format(user.id))
    return redirect('/admin')

@app.route('/recover', methods=['GET', 'POST'])
@login_required
def recover_user():
    form = forms.RemindUser()

    if request.form and form.validate():
        user = User.query.filter_by(username=request.form['password']).first()
        if not user:
            flash('Email address not found.')
            return render_template('recover_password.html', form=form)
        msg = Message("Tortworth Matrix Password Recovery",
                      sender="ME@ME.COM",
                      recipients=[user.username])
        password = user.password
        msg.html = (
            "<h2>Your password is: <b>{password}</b></h1><br><br>"
            ":D".format(password=password)
        )

        mail.send(msg)
        flash('Please check your emails.')
        return render_template('landing.html')

    return render_template('recover_password.html', form=form, username=session['username'])

@app.route('/search')
@login_required
def search():
    username = current_user.username

    return render_template('search.html',
                           username=username,
                           greeting=utils.greeting(),
                           logout=True,
                           dad_joke=utils.get_dad_joke())


@app.route('/handle_data', methods=['POST'])
@login_required
def handle_data():
    submit = request.form.get('submit')
    print_map = request.form.get('print')
    room = request.form['Room']
    room = room.lstrip('0')

    room_data = utils.get_room_data()

    room_list = []
    for x in room_data:
        room_list.append(x)

    if room == '':
        flash('No room entered')
        return redirect('/search')
    elif all(x.isalpha() or x.isspace() for x in room):
        flash('Please enter room number, not name.')
        return redirect('/search')
    elif room not in room_list:
        flash('Room not found')
        return redirect('/search')
    else:
        if print_map:
            flash('Maps are under construction.')
            return home()
        else:
            return redirect('/room/{}'.format(room))


@app.route('/edit/<room_number>', methods=['GET', 'POST'])
@login_required
def edit(room_number):
    form = forms.AddNotes()

    file = utils.load_json(os.path.abspath('static/room_list.json'))
    note_list = file[room_number]['notes']

    if request.form and form.validate():
        file[room_number]['notes'].append(request.form['notes'])
        flash('Note added.')
        utils.save_json(os.path.abspath(
            'static/room_list.json'), file)
        return redirect('/edit/{}'.format(room_number))

    return render_template('edit.html', note_list=note_list, room=room_number, form=form, logout=True)


@app.route('/delete_note/<room_number>/<note>')
@login_required
def delete_note(room_number, note):
    file = utils.load_json(os.path.abspath('static/room_list.json'))

    file[room_number]['notes'].remove(note)

    utils.save_json(os.path.abspath('static/room_list.json'), file)
    flash('Note Deleted.')
    return redirect(f'/edit/{room_number}')


@app.route('/room/<room_number>')
@login_required
def room(room_number):
    room_data = utils.get_room_data()

    try:
        room_info = utils.get_room_info(room_data[room_number])
    except KeyError:
        flash('Room not found.')
        return redirect('search')

    room_info_colors = utils.get_room_colors(room_info)

    try:
        room_name = utils.ROOM_NAMES[int(room_number)]
    except KeyError:
        room_name = False

    all_images = UploadedImage.query.filter_by(room=room_number).all()
    length = len(all_images)
    file_paths = []
    for x in all_images:
        file_paths.append(f"/static/uploads/{x.file_name}")

    if length == 0:
        file_paths = False

    return render_template(
        'room.html',
        room=room_number,
        images=file_paths,
        greeting=utils.greeting(),
        room_name=room_name,
        logout=True,
        username=session.get('username'),
        **room_info, **room_info_colors)