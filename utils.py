import json
from datetime import datetime
import os
from random import randint
import requests


# def nice_date():
#     date = datetime.utcnow()
#     date = date.strftime("%d%m%Y")
#     date = datetime.strptime(str(date), "%d%m%Y")
#     return date

ROOM_NAMES = {
    1 : 'Teulon',
    25 : 'Paxton',
    26 : 'Kipling',
    28 : 'Drawing Room'
}

def login_user(session, user):
    session['logged_in'] = True
    session['username'] = (user.title())
    return session



def save_json(filename, data):
    """Atomically saves json file"""
    rnd = randint(1000, 9999)
    path, ext = os.path.splitext(filename)
    tmp_file = "{}-{}.tmp".format(path, rnd)
    _save_json(tmp_file, data)
    read_json(tmp_file)
    os.replace(tmp_file, filename)
    return True

def _save_json(filename, data):
    with open(filename, encoding='utf-8', mode="w") as f:
        json.dump(data, f, indent=4,sort_keys=True,
            separators=(',',' : '))
    return data

def load_json(filename):
    """Loads json file"""
    return read_json(filename)

def read_json(filename):
    with open(filename, encoding='utf-8', mode="r") as f:
        data = json.load(f)
    return data

def get_dad_joke():
        
    r = requests.get('https://icanhazdadjoke.com/',
                    headers={'Accept': 'text/plain'})
    try:
        return r.text
    except:
        return False


def greeting():
    currentTime = datetime.now()
    if currentTime.hour < 12:
        greeting = 'Good morning.'
    elif 12 <= currentTime.hour < 18:
        greeting = 'Good afternoon.'
    else:
        greeting = 'Good evening.'
    
    return greeting

def get_room_info(room):
    notes = []
    for x in room.get('notes'):
        notes.append(x)

    room_info = {
    "floor": room.get('floor'),
    "room_type": room.get('room_type'),
    "view": room.get('view'),
    "desk": room.get('desk'),
    "armchair": room.get('armchair'),
    "sitting_area": room.get('sitting_area'),
    "coffe_table": room.get('coffe_table'),
    "fridge": room.get('fridge'),
    "full_mirror": room.get('full_mirror'),
    "shower_bath": room.get('shower_bath'),
    "roll_top_bath": room.get('roll_top_bath'),
    "bathroom_windows": room.get('bathroom_windows'),
    "connecting": room.get('connecting'),
    "size": room.get('size'),
    "sofa_bed" : room.get('sofa_bed'),
    "notes": notes
    }
    return room_info

def get_room_data():
    with open(os.path.abspath('static/room_list.json')) as room_list:
        room_data = json.load(room_list)
    return room_data

def get_room_colors(room_info):
    room_info_colors = {}
    for k, v in room_info.items():
        if v == "Yes":
            room_info_colors[k + '_color'] = 'check'
        else:
            room_info_colors[k + '_color'] = 'times'
    
    return room_info_colors

#  Get User
def get_user(user):
    with open(os.path.abspath('tortworth/static/room_list.json'),'r') as f:
        user_list = json.load(f)
        if user in user_list: #have this check password instead
            return True
        else:
            return False