from app import db
import datetime
from flask_login import UserMixin


class User(UserMixin, db.Model):
    """ Create user table"""
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))
    job_title = db.Column(db.String(80))
    nickname = db.Column(db.String(80))
    admin = db.Column(db.Boolean())
    posts = db.relationship('Post', backref='author', lazy='dynamic')


    def __init__(self, username, password, job_title, admin, nickname):
        self.username = username
        self.password = password
        self.job_title = job_title
        self.admin = admin
        self.nickname = nickname
    
    def __repr__(self):
        return f'<User: {self.username}'

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sentiment = db.Column(db.String(80))
    department = db.Column(db.String(80))
    guest_name = db.Column(db.String(280))
    arrival_date = db.Column(db.DateTime)
    body = db.Column(db.String(280))
    timestamp = db.Column(db.Date, index=True, default=datetime.date.today() )
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

class UploadedImage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    room = db.Column(db.Integer)
    file_name = db.Column(db.String(1000))
    file_path = db.Column(db.String(1000))
    
    def __init__(self, room, file_name, file_path):
        self.room = room
        self.file_name = file_name
        self.file_path = file_path