import json
from tortworth import utils


def logged_in(session):
    """
    Function to check if user is logged in.
    """
    if session.get('logged_in'):
        return True
    else:
        return False

def get_user_name(user):
    users = utils.load_json('static/users.json')

    email = user['name']

    username = users[email]['username']

    return username

def get_user_password(user):
    users = utils.load_json('static/users.json')

    email = user[' ']

    username = users[email]['username']

    return username    
    
def get_user(user, password):
    """
    Takes 
    user: form username
    password: form password

    checks against json file
    """
    with open('static/users.json','r') as f:
        user_list = json.load(f)

    if user in user_list: #have this check password instead
        password_data = user_list[user]['password']
        if password == password_data:
            return True
        else:
            return False
    else:
        return False

def update_user_password(form, session):
    """updates user and takes form obj"""
    users = utils.load_json('static/users.json')

    for k, v in users.items():
        # print(session['username'] + v['username'])
        if session['username'].lower() == v['username'].lower():
            email = k               
    raw_username = email.split('.')
    username = raw_username[0]

    users[email]['password'] = form['password']
    users[email]['username'] = username

    utils.save_json('static/users.json', users)

## Add recover password function


def new_user(form):
    """
    Adds new user to file, takes whole file object
    """
    users = utils.load_json('static/users.json')
    email = form['email'].lower()
    raw_username = email.split('.')
    username = raw_username[0]

    new_user = {
        email : {
            "password" : form['password'],
            "username" : username}
    }

    users.update(new_user)

    utils.save_json('static/users.json', users)

    return new_user