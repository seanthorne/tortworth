$(window).on('load', function () {
    // Animate loader off screen
    $(".loader").fadeOut('slow')

});

// removes sidebar on tablet and lower
const mq = window.matchMedia( "(min-width: 800px)" );
if (mq.matches) {
    // window width is at least 700px
    if (sessionStorage.getItem('sidebar')) {
        var data = sessionStorage.getItem('sidebar');
        if (data === 'closed') {
            $(".page-wrapper").removeClass("toggled");
        }
    }
    } else {
         // window width is less than 800px
        $(".page-wrapper").removeClass("toggled");
        $(".postBody").addClass('pb-3')
        $(".guestDetails").addClass("border-top pt-3").removeClass("border-left")
   
    }

// Wrap every letter in a span
$('.ml9 .letters').each(function(){
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter user-name'>$&</span>"));
  });
  
  anime.timeline({loop: true})
    .add({
      targets: '.ml9 .letter',
      scale: [0, 1],
      duration: 700,
      elasticity: 600,
      delay: function(el, i) {
        return 45 * (i+1)
      }
    }).add({
      targets: '.ml9',
      opacity: 0,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000
    });

    
// $(document).ready(function () {
//     if (sessionStorage.getItem('sidebar')) {
//         var data = sessionStorage.getItem('sidebar');
//         if (data === 'closed') {
//             $(".page-wrapper").removeClass("toggled");
//         }
//     }
// });

jQuery(function ($) {

    // Dropdown menu
    $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200);
        if ($(this).parent().hasClass("active")) {
            $(".sidebar-dropdown").removeClass("active");
            $(this).parent().removeClass("active");
        } else {
            $(".sidebar-dropdown").removeClass("active");
            $(this).next(".sidebar-submenu").slideDown(200);
            $(this).parent().addClass("active");
        }

    });

    // close sidebar 
    $("#close-sidebar").click(function () {
        sessionStorage.setItem('sidebar', 'closed');
        console.log('Closed the sidebar')
        var data = sessionStorage.getItem('sidebar')
        console.log(data)
        $(".page-wrapper").removeClass("toggled");
    });

    //show sidebar
    $("#show-sidebar").click(function () {
        sessionStorage.setItem('sidebar', 'open');
        console.log('Opened the sidebar')
        var data = sessionStorage.getItem('sidebar')
        console.log(data)
        $(".page-wrapper").addClass("toggled");
    });

    //switch between themes 
    var themes = "chiller-theme ice-theme cool-theme light-theme";
    $('[data-theme]').click(function () {
        $('[data-theme]').removeClass("selected");
        $(this).addClass("selected");
        $('.page-wrapper').removeClass(themes);
        $('.page-wrapper').addClass($(this).attr('data-theme'));
    });

    // switch between background images
    var bgs = "bg1 bg2 bg3 bg4";
    $('[data-bg]').click(function () {
        $('[data-bg]').removeClass("selected");
        $(this).addClass("selected");
        $('.page-wrapper').removeClass(bgs);
        $('.page-wrapper').addClass($(this).attr('data-bg'));
    });

    // toggle background image
    $("#toggle-bg").change(function (e) {
        e.preventDefault();
        $('.page-wrapper').toggleClass("sidebar-bg");
    });

    //custom scroll bar is only used on desktop
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $(".sidebar-content").mCustomScrollbar({
            axis: "y",
            autoHideScrollbar: true,
            scrollInertia: 300
        });
        $(".sidebar-content").addClass("desktop");

    }
});