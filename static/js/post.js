$('#logBody').on("input", function () {
    var dInput = this.value;
    console.log(dInput);
    $('#entryBody').text(dInput);
});
$('#guestName').on("input", function() {
    $('#guestProfile').removeClass("d-none");
    var guestName = this.value;
    $('#guestNameEntry').text(guestName);
    
});



// Element.prototype.remove = function() {
//     this.parentElement.removeChild(this);
// }

// function deletePost(post){
//     console.log(post)
//     document.getElementById(String(post)).remove();
// }

function deletePost(post){
    $(post).fadeToggle('slow', function(){ $target.remove(); });
    // $(post).remove();
};

$('#entryDepartment').on('change', function(){
    var entryDepartment = this.value;

    if($(this).val()){
    $('#footerDepartment').text(this.value)
    }
});
$('#entryArrivalDate').on('input', function(){
    var arrival = this.value;
    $('#guestProfile').removeClass("d-none");
    $('#arrivalDate').text(arrival).removeClass('d-none');
})

$('#entrySentiment').on('change', function () {
    if ($(this).val() == "Negative") {
        $('#footerSentiment').text("Negative").addClass('text-danger').removeClass('text-success text-warning');
        $('#Entry').removeClass("border-success");
        $('#Entry').removeClass("border-warning");
        $('#Entry').addClass("border-danger");
    }

    else if ($(this).val() == "Positive") {
        $('#footerSentiment').removeClass("text-danger text-warning");
        $('#footerSentiment').text("Positive").addClass('text-success');
        $('#Entry').removeClass("border-warning border-danger");
        $('#Entry').addClass("border-success");
    }
    else{
        $('#footerSentiment').removeClass('text-danger text-warning')
        $('#footerSentiment').text("Neutral").addClass('text-warning');
        $('#Entry').addClass("border-warning");}
}
);
